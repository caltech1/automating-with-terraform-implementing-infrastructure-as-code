provider "aws" {
    region = "us-west-1"
    access_key = "AKIA33CO4ALPUCCTZIM6"
    secret_key = "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"
}

resource "aws_vpc" "development-vpc" { 
    cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.development-vpc.id
    cidr_block = "10.0.10.0/24"
    availability_zone = "us-west-1a"
}

resource "aws_instance" "myapp-server" {
    ami = "ami-02d03ce209db75523"
    instance_type = "t2.micro" 
    
    subnet_id = aws_subnet.dev-subnet-1.id
    vpc_security_group_ids = [aws_security_group.myapp-sg.id]
    availability_zone = "us-west-1a"

    associate_public_ip_address = true
    key_name = "server-key-pair1"

    user_data = <<EOF
                    #!/bin/bash
                    sudo yum update -y && 
                    sudo yum install-y openjdk-8-jdk &&
                    sudo yum install-y python3.7 && 
                    sudo yum install-y jenkins
                EOF
}

resource "aws_security_group" "myapp-sg" {
    name = "myapp-sg"
    vpc_id = aws_vpc.development-vpc.id 

    ingress { 
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["98.42.134.210/32"]
    }

    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        prefix_list_ids = []
    }
} 


