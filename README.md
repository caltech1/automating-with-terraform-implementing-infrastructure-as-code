# Automating With Terraform - Implementing Infrastructure as Code


## Project Description 
- This project illustrates Infrastructure as Code (IAC) in its most rudimentary and utilitarian form. 
- The associated Terraform code creates:
    * A Virtual Private Cloud (i.e. virtual network)
    * A Subnet within the VPC 
    * Associated security group for the VPC and Subnet
        - Ingress attribute
        - Egress attribute 
    * EC2 Instance Creation
        - Pubic and private key origin
    * Provisioner 
        - Associate application installation 
            * Python
            * Open JDK
            * Jenkins

## Utility
- The code was written for a social networking startup that wants to provision AWS Servers on an ad-hoc basis. Primarily for testing. 
- The code allows quick provisioning of a single AWS server and installation of desired applciations. 

## Narrative 
- A detailed narrative of the code's creation is found in the repository PDF titled, " Automating With Terraform: Implementing Infrastructure as Code "





